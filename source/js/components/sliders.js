$(document).ready(function () {
	var swiperWelcome = new Swiper('.swiper-welcome', {
		slidesPerView: 1,
		spaceBetween: 30,
		loop: true,
		pagination: {
			el: '.swiper-welcome-pagination',
			clickable: true,
		},
		navigation: {
			nextEl: '.swiper-welcome-button-next',
			prevEl: '.swiper-welcome-button-prev',
		},
	})

	var swiperHotProduct = new Swiper('.swiper-hot-product', {
		effect: "slide",
		slidesPerView: 1,
		spaceBetween: 35,
		loop: true,
		navigation: {
			nextEl: '.swiper-hot-product-button-next',
			prevEl: '.swiper-hot-product-button-prev',
		},
		breakpoints: {
			500: {
			  slidesPerView: 2,
			},
			768: {
			  slidesPerView: 3,
			},
			1024: {
			  slidesPerView: 5,
			},
		}
	})
	
    var swiperBrend = new Swiper('.swiper-brend', {
		slidesPerView: 2,
		spaceBetween: 20,
		loop: true,
		navigation: {
			nextEl: '.swiper-brend-button-next',
			prevEl: '.swiper-brend-button-prev',
		},
		breakpoints: {
			768: {
			  slidesPerView: 3,
			},
			1024: {
			  slidesPerView: 6,
			},
		}
	})


	var galleryThumbs = new Swiper('.gallery-thumbs', {
		direction: 'vertical',
		lazy: true,
		spaceBetween: 20,
		slidesPerView: 4,
		loop: true,
		freeMode: true,
		preloadImages: true,
		watchSlidesVisibility: true,
		watchSlidesProgress: true,
		draggable: false,
		
	  });
	  var galleryTop = new Swiper('.gallery-top', {
		spaceBetween: 10,
		lazy: true,
		preloadImages: true,
		loop: true,
		navigation: {
		  nextEl: '.swiper-button-next',
		  prevEl: '.swiper-button-prev',
		},
		thumbs: {
		  swiper: galleryThumbs,
		},
	  });


});
