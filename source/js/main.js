'use strict';

//= ../node_modules/jquery/dist/jquery.js
//= ../node_modules/bootstrap/dist/js/bootstrap.js
//= ../node_modules/lazysizes/lazysizes.js
//= ../node_modules/swiper/swiper-bundle.js

//= components/sliders.js
//= library/wow.js
//= library/jquery-ui.js




$(document).ready(function () {

	new WOW().init({
		mobile: false
	});


	// START MENU
	var body = document.querySelector("#page");
	var burger = document.querySelector("#burger");
	var modalMenu = document.querySelector(".menu-modal");
	var modalMenuContent = modalMenu.querySelector(".menu-modal__content");
	var modalMenuClose = modalMenu.querySelector(".menu-modal__close");
	var minicart = document.querySelector(".minicart__link_btn");

	burger.addEventListener("click", function (e) {
		modalMenu.classList.add("menu-modal_active");
		boduHidden();
	});
	modalMenuClose.addEventListener("click", function (e) {
		modalMenu.classList.remove("menu-modal_active");
		bodyDefault();
	});
	modalMenu.addEventListener("click", function (e) {
		modalMenu.classList.remove("menu-modal_active");
		bodyDefault();
	});
	modalMenuContent.addEventListener("click", function (e) {
		e.stopPropagation();
	});
	// END MENU


	// START minicart
	var minicart = document.querySelector(".minicart__link_btn");
	var modalMininCart = document.querySelector(".minicart__dropdown");
	var modalMininCartContent = modalMininCart.querySelector(".minicart__dropdown_content");
	var modalCloseMiniCart = modalMininCart.querySelector(".minicart__dropdown_close");
	
	minicart.addEventListener("click", function (e) {
		modalMininCart.classList.add("minicart__dropdown_active");
		boduHidden();
	});
	modalCloseMiniCart.addEventListener("click", function (e) {
		modalMininCart.classList.remove("minicart__dropdown_active");
		bodyDefault();
	});
	modalMininCart.addEventListener("click", function (e) {
		modalMininCart.classList.remove("minicart__dropdown_active");
		bodyDefault();
	});
	modalMininCartContent.addEventListener("click", function (e) {
		e.stopPropagation();
	});
	// END minicart

	function boduHidden() {
		body.classList.add("modal-open");
	}
	function bodyDefault() {
		body.classList.remove("modal-open");
	}






	// START btn-to-top
	var goTopBtn = document.querySelector('.btn-to-top');
	window.addEventListener('scroll', trackScroll);
	goTopBtn.addEventListener('click', backToTop);

	function trackScroll() {
		var scrolled = window.pageYOffset;
		var coords = document.documentElement.clientHeight;

		if (scrolled > coords) {
			goTopBtn.classList.add('btn-to-top_show');
		}
		if (scrolled < coords) {
			goTopBtn.classList.remove('btn-to-top_show');
		}
	}

	function backToTop() {
		if (window.pageYOffset > 0) {
			window.scrollBy(0, -80);
			setTimeout(backToTop, 0);
		}
	}
	// END btn-to-top
	

	// start slider price
	var sliderrange = jQuery('#slider-range');
	// var amountprice = jQuery('#amount');
	jQuery(function() {
		sliderrange.slider({
			range: true,
			min: 0,
			max: 20000,
			values: [0, 20000],
			slide: function(event, ui) {
				var $Handle = sliderrange.find('.ui-slider-handle');
				$Handle.css('margin-left', -1 * $Handle.width() * (sliderrange.slider('value') / sliderrange.slider('option', 'max')));
				
				//console.log(sliderrange.slider("values", 0));
				// amountprice.val("$" + sliderrange.slider("values", 0) + " - $" + sliderrange.slider("values", 1));
			}
		});
		var $Handle = sliderrange.find('.ui-slider-handle');
		$Handle.css('margin-left', -1 * $Handle.width() * (sliderrange.slider('value') / sliderrange.slider('option', 'max')));
	});
	

	// end slider price


	/*----------------------------
	Cart Plus Minus Button
	------------------------------ */
	var CartPlusMinus = jQuery('.cart-plus-minus');
    CartPlusMinus.prepend('<div class="dec qtybutton">-</div>');
    CartPlusMinus.append('<div class="inc qtybutton">+</div>');
    jQuery(".qtybutton").on("click", function () {
        var $button = jQuery(this);
        var target = $button.parent().find("input")
        var oldValue = target.val();
        if ($button.hasClass('inc')) {
            var newVal = parseFloat(oldValue) + 1;
        } else if ($button.hasClass('dec')) {
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }

        target.val(newVal).change();
    });


});
